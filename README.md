# Summary

This role setups up a WordPress site that is connected to a MySQL database

This role is used to setup the following:

* Installs wordpress
* Sets up Traefik configurations
* Sets up external DNS through Cloudflare
* Sets up internal DNS through Technitium

Assumes:

* Routing to the database is internal only
* Traefik is already installed

## Dependent projects

| Project Name             |                                                                                                   |
|--------------------------|---------------------------------------------------------------------------------------------------|
| app-server-mysql         | Used to create a new database to be used by the wordpress install                                 |
| app-server-traefik       | Uses a shared task to generate labels to be used in the container                                 |

## Variables

| Configuration            | Description                                                                                       |
|--------------------------|---------------------------------------------------------------------------------------------------|
| wordpress_project_name   |                                                                                                   |

## Application Settings

| Variable Name            | Description                                                                                       |
|--------------------------|---------------------------------------------------------------------------------------------------|
| wordpress_application    | Description                                                                                       |
|   name                   | Name of the container                                                                             |
|   version                | Version of the container                                                                          |
|   site_url               | URL for the site                                                                                  |

## Infrastructure Settings

| Variable name            | Description                                                                                       |
|--------------------------|---------------------------------------------------------------------------------------------------|
| wordpress_infrastructure | Description                                                                                       |
|   install_type           | Type of install. This is typically docker                                                         |
|   subdomain              | Subdomain for the site                                                                            |
|   dns_zone               | DNS Zone for the site                                                                             |
|   protocol_type          | Protocol type for the site. This is typically https or http                                       |
|   target_server          | Host name for the server the site will reside on. Also used for setting up the CNAME Record       |
|   access_type            | Define if this is for internal or external access                                                 |
|   web_port               | Port for the web interface. Has to be specified                                                   |
|   container_name         | Name of the container to be set up                                                                |
|   data_directory         | Default directory for key data stored for wordpress                                               |
|   config_directory       | Default directory for config files for wordpress                                                  |
|   backup_directory       | Default directory for backups for wordpress                                                       |
|   log_directory          | Default directory for log files                                                                   |
|   proxy_type             | Proxy type for the site. This is typically traefik                                                |

## Database Settings

| Variable name            | Description                                                                                       |
|--------------------------|---------------------------------------------------------------------------------------------------|
| wordpress_db             | wordpress database configurations                                                                 |
|  host_name               | Server host name                                                                                  |
|  host_port               | Port exposed on the server                                                                        |
|  type                    | This is always mysql                                                                              |
|  name                    | This is the database name for the app                                                             |
|  user_name               | User name for the application database                                                            |
|  user_password           | Password for the application database                                                             |
|  root_user               | root user name to create the database                                                             |
|  root_password           | root password to create the database                                                              |
