---
  - name: Check to see if the mysql dump file required has already been downloaded
    stat: path={{ temp_folder }}/wordpress_wip/{{ site_name }}.sql
    register: downloaded_mysql_backup_file

  - name: Copy database backup from local to remote
    become: yes
    become_method: sudo
    copy: src={{ local_project_db_import }} dest={{ temp_folder }}/wordpress_wip/{{ site_name }}.sql
          mode=0755
    when: downloaded_mysql_backup_file.stat.exists == False

  - name: Import existing database
    mysql_db: name={{ site_database_name }} login_host={{ site_database_server }}
              login_user={{ app_create_username }} login_password={{ app_create_userpassword }}
              target={{ temp_folder }}/wordpress_wip/{{ site_name }}.sql state=import

  - name: Check to see if the project file required has already been downloaded
    stat: path={{ download_folder }}/downloaded_wp_project.tar.gz
    register: downloaded_project_file

  - name: Copy archived folder from local machine to the remote download folder
    become: yes
    become_method: sudo
    copy: src={{ local_project_file }} dest={{ download_folder }}/downloaded_wp_project.tar.gz
          owner={{ deployadmin_username }} group={{ deployadmin_username }} mode=0644
    when: downloaded_project_file.stat.exists == False

  - name: Create temporary folder
    become: yes
    become_method: sudo
    file: path={{ temp_folder }}/wordpress_wip/ state=directory
          owner={{ deployadmin_username }} group={{ deployadmin_username }} mode=0755

  - name: Unpack the archived folder
    become: yes
    become_method: sudo
    unarchive: src={{ download_folder }}/downloaded_wp_project.tar.gz dest={{ temp_folder }}/wordpress_wip/
               copy=no

  - name: Copy downloaded folder to the web folder
    become: yes
    become_method: sudo
    command: mv {{ temp_folder }}/wordpress_wip/{{ local_project_directory }} {{ website_directory }}/{{ site_directory }}

  - name: Backup config file
    become: yes
    become_method: sudo
    command: cp {{ website_directory }}/{{ site_directory }}/wp-config.php {{ website_directory }}/{{ site_directory }}/wp-config.php.backup

  - name: Update wp-config file with appropriate variables
    become: yes
    become_method: sudo
    lineinfile: dest={{ website_directory }}/{{ site_directory }}/wp-config.php regexp="^define\('{{ item.variable_name }}'"
                line="define('{{ item.variable_name }}', '{{ item.variable_value }}');" state=present
    with_items:
    - { variable_name: 'DB_NAME', variable_value: "{{ site_database_name }}" }
    - { variable_name: 'DB_USER', variable_value: "{{ site_database_user }}" }
    - { variable_name: 'DB_PASSWORD', variable_value: "{{ site_database_password }}" }
    - { variable_name: 'DB_HOST', variable_value: "{{ site_database_server }}" }
